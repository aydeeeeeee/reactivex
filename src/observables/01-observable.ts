import { Observable, Observer } from 'rxjs';

/* Another way */
const observer: Observer<any> = {
  next: value => console.log('siguiente [next]:', value),
  error: error => console.warn('error [obs]:', error),
  complete: () => console.info('Completado [obs]')
}


// const obs$ = Observable.create();

const obs$ = new Observable<string>(subs => { //subscriber

  subs.next('Holaaa');
  subs.next('Mundo!');

  // Forzar error
  // const a = undefined;
  // a.nombre = 'Aydee';

  subs.complete();

});

obs$.subscribe(observer);

/* obs$.subscribe(
  valor => console.log('next:', valor), // can be (console.log) too.
  error => console.warn('error:', error),
  () => console.info('Completado')
)
 */


