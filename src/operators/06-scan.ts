import { from } from 'rxjs';
import { reduce, scan, map } from 'rxjs/operators';

const numbers = [1, 2, 3, 4, 5];

const totalAcumulator = (acc, curr) => acc + curr;

// Reduce
from(numbers).pipe(
  reduce(totalAcumulator, 0)
).subscribe(console.log);

//scan
// acumulacion en cada iteración
from(numbers).pipe(
  scan(totalAcumulator, 0)
).subscribe(console.log);

//Redux
interface Usuario {
  id?: string,
  auth?: boolean,
  token?: string,
  age?:number
}

const user: Usuario[] = [
  {
    id: 'ad',
    auth: false,
    token: null
  },
  {
    id: 'ad',
    auth: true,
    token: 'lol'
  },
  {
    id: 'ad',
    auth: true,
    token: 'abc123'
  }
];

const state$ = from(user).pipe(
  scan<Usuario>((acc, curr) => {
    return { ...acc, ...curr }
  }, {age: 25})
);

const id$ = state$.pipe(
  map( state =>  state.id )
);

id$.subscribe(console.log);