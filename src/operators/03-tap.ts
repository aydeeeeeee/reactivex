import { range, from, fromEvent } from 'rxjs';
import { tap, map } from 'rxjs/operators';

const numbers$ = range(1, 5);

/**
 * tap: para depurar
 */

numbers$.pipe(
  tap(x => console.log('antes', x)),
  map(val => val*10),
  tap({
    next: valor => console.log('despues', valor),
    complete: () => console.log('Se terminó todo')
  })
)
  .subscribe(val => console.log('subs', val))